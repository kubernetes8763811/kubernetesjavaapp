FROM openjdk:23-ea-jdk-slim-bullseye

WORKDIR /app

COPY build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar .

# create group and user
RUN groupadd -r dockerjavaapp && useradd -g dockerjavaapp dockerjavaapp

# set ownership and permission
RUN chown -R dockerjavaapp:dockerjavaapp /app

# switch user
USER dockerjavaapp


CMD ["java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]